# Talisman ESB Connector
This library allows you to connect to the Talisman ESB and use a wide range of functionality.

## Installation

Add the following to the ``repositories`` block in your ``composer.json``:

```
{
    "type": "vcs",
    "url": "https://bitbucket.org/redkiwi-sites/typo3-projects-talisman-esb-connector.git"
}
```

And run: ``composer require grandslam/talisman-esb-connector dev-master``

## Examples
To run the examples, add your private key to ``Examples/privkey.pem`` and run: ``php vendor/grandslam/talisman-esb-connector/Examples/<file>.php``
