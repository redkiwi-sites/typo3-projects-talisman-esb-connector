<?php
require __DIR__ . '/../../../autoload.php';

use GrandSlam\TalismanEsbConnector\Models;
use GrandSlam\TalismanEsbConnector\Service\ClientService;
use GrandSlam\TalismanEsbConnector\EsbConnector;

$esbConnector = new EsbConnector(
    'privatekey.pem',
    'development',
    'http://talisman-redesign-esb.dev.local/'
);

$clientService = new ClientService();
$clientService->setEsbConnector($esbConnector);

$result = $clientService->checkClientExistence('talisman');

if (count($result) > 0) {
    echo 'Client(s) found with ID(s):' . "\n";
    foreach ($result as $clientId) {
        echo $clientId . "\n";
    }
} else {
    echo 'Client not found.';
}
