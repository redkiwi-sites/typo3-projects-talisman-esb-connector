<?php
require __DIR__ . '/../../../autoload.php';

use GrandSlam\TalismanEsbConnector\Service\ClientService;
use GrandSlam\TalismanEsbConnector\Models\Brochure;
use GrandSlam\TalismanEsbConnector\EsbConnector;

$esbConnector = new EsbConnector(
    'privatekey.pem',
    'development',
    'http://talisman-redesign-esb.dev.local/'
);

$clientService = new ClientService();
$clientService->setEsbConnector($esbConnector);

$brochure = new Brochure();

$brochure->setComponentIds([106,107]);
$brochure->setBrochureSubject('Test Brochure Subject');
$brochure->setBrochureText('Test Brochure Text');
$brochure->setClientId(1);

try {
    $brochureId = $clientService->addBrochure($brochure);
    echo 'Brochure successfully added to client.';
} catch (\Exception $e) {
    echo 'An error occurred while adding brochure: ' . $e->getMessage();
}
