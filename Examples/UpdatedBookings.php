<?php
require __DIR__ . '/../../../autoload.php';

date_default_timezone_set('Europe/Amsterdam');

use GrandSlam\TalismanEsbConnector\EsbConnector;
use GrandSlam\TalismanEsbConnector\Service\UpdatedBookingsService;

$esbConnector = new EsbConnector(
    __DIR__ . '/privatekey.pem',
    'development',
    'http://talisman-redesign-esb.dev.local/'
);

$updatedBookingsService = new UpdatedBookingsService();
$updatedBookingsService->setEsbConnector($esbConnector);

// Updated bookers since 2 days
$endDate = new \DateTime('now');
$startDate = new \DateTime('-2 days');

$updatedBookers = $updatedBookingsService->get([
    'startDate' => $startDate->getTimestamp(),
    'endDate' => $endDate->getTimestamp()
]);

print_r($updatedBookers);
