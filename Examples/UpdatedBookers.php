<?php
require __DIR__ . '/../../../autoload.php';

date_default_timezone_set('Europe/Amsterdam');

use GrandSlam\TalismanEsbConnector\EsbConnector;
use GrandSlam\TalismanEsbConnector\Service\UpdatedBookersService;

$esbConnector = new EsbConnector(
    __DIR__ . '/privatekey.pem',
    'development',
    'http://talisman-redesign-esb.dev.local/'
);

$updatedBookersService = new UpdatedBookersService();
$updatedBookersService->setEsbConnector($esbConnector);

// Updated bookers since 2 days
$endDate = new \DateTime('now');
$startDate = new \DateTime('-2 days');

$updatedBookers = $updatedBookersService->get([
    'startDate' => $startDate->getTimestamp(),
    'endDate' => $endDate->getTimestamp()
]);

print_r($updatedBookers);
