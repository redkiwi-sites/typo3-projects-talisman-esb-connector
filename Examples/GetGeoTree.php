<?php
require __DIR__ . '/../../../autoload.php';

use GrandSlam\TalismanEsbConnector\EsbConnector;
use GrandSlam\TalismanEsbConnector\Models\GeoTreeContinent;
use GrandSlam\TalismanEsbConnector\Models\GeoTreeCountry;
use GrandSlam\TalismanEsbConnector\Service\GeoTreeService;

$esbConnector = new EsbConnector(
    'privatekey.pem',
    'development',
    'http://talisman-redesign-esb.dev.local/'
);

$geoTreeService = new GeoTreeService();
$geoTreeService->setEsbConnector($esbConnector);

$continents = $geoTreeService->get();

foreach ($continents as $continent) {
    /** @var GeoTreeContinent $continent */
    echo $continent->getName() . ":\n";
    foreach ($continent->getChildren() as $country) {
        /** @var GeoTreeCountry $country */
        echo "\t\t" . $country->getName() . "\n";
    }
}
