<?php
require __DIR__ . '/../../../autoload.php';

use GrandSlam\TalismanEsbConnector\Service\ClientService;
use GrandSlam\TalismanEsbConnector\Models\Note;
use GrandSlam\TalismanEsbConnector\EsbConnector;

$esbConnector = new EsbConnector(
    'privatekey.pem',
    'development',
    'http://talisman-redesign-esb.dev.local/'
);

$clientService = new ClientService();
$clientService->setEsbConnector($esbConnector);

$note = new Note();

$note->setNoteSubject('Test subject');
$note->setNoteText('Test note body text');
$note->setNoteType('brochure'); // "brochure" or "quote"
$note->setRequestedDate(time());

$clientId = 1;

try {
    $noteId = $clientService->addNote($clientId, $note);
    echo 'Note ' . $noteId . ' successfully added to client.';
} catch (\Exception $e) {
    echo 'An error occurred while adding note: ' . $e->getMessage();
}
