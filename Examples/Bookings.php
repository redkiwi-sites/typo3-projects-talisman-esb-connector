<?php
require __DIR__ . '/../../../autoload.php';

date_default_timezone_set('Europe/Amsterdam');

use GrandSlam\TalismanEsbConnector\Service\BookingsService;
use GrandSlam\TalismanEsbConnector\Service\BookingService;
use GrandSlam\TalismanEsbConnector\EsbConnector;
use GrandSlam\TalismanEsbConnector\Service\BookerService;

$esbConnector = new EsbConnector(
    __DIR__ . '/privatekey.pem',
    'development',
    'http://talisman-redesign-esb.dev.local/'
);

$bookingsService = new BookingsService();
$bookingsService->setEsbConnector($esbConnector);

// August 1st 2016 - August 16th 2016
$bookingsAugust = $bookingsService->get([
    'startDate' => '1470009600',
    'endDate' => '1471305600'
]);

// September 1st 2016 - September 16th 2016
$bookingsSeptember = $bookingsService->get([
    'startDate' => '1472688000',
    'endDate' => '1473984000'
]);

echo 'Bookings August 1st 2016 - August 16th 2016: ' . count($bookingsAugust['additionalData']['bookings']['booking']) . "\n";
foreach ($bookingsAugust['additionalData']['bookings']['booking'] as $booking) {
    $bookingDate = new \DateTime();
    $bookingDate->setTimestamp($booking['bookingDate']);
    echo ' - Booking date: ' . $bookingDate->format('d-m-Y') . "\n";
}
echo 'Bookings September 1st 2016 - September 16th 2016: ' . count($bookingsSeptember['additionalData']['bookings']['booking']) . "\n";
foreach ($bookingsSeptember['additionalData']['bookings']['booking'] as $booking) {
    $bookingDate = new \DateTime();
    $bookingDate->setTimestamp($booking['bookingDate']);
    echo ' - Booking date: ' . $bookingDate->format('d-m-Y') . "\n";
}

$bookingService = new BookingService();
$bookingService->setEsbConnector($esbConnector);

$booking = $bookingService->get([
    'bookingReferenceNumber' => 'TR160624'
]);

$bookerService = new BookerService();
$bookerService->setEsbConnector($esbConnector);

$booker = $bookerService->get([
    'bookerId' => '2303'
]);

print_r($booking);
print_r($booker);
