<?php
require __DIR__ . '/../../../autoload.php';

use GrandSlam\TalismanEsbConnector\Service\ClientService;
use GrandSlam\TalismanEsbConnector\EsbConnector;
use GrandSlam\TalismanEsbConnector\Models\Client;

$esbConnector = new EsbConnector(
    'privatekey.pem',
    'development',
    'http://talisman-redesign-esb.dev.local/'
);

$clientService = new ClientService();
$clientService->setEsbConnector($esbConnector);

$client = new Client();

$client->setId(6277);
$client->setEmailAddress('mbrandt@talisman.nl');
$client->setFirstName('Monique');
$client->setLastName('Brandt');

try {
    $statusCode = $clientService->update($client);
    switch ($statusCode) {
        case 200:
            echo 'Client updated successfully';
            break;
        case 403:
            echo 'Client update failed, access denied';
            break;
        case 404:
            echo 'Client update failed, not found';
            break;
        case 500:
            echo 'Client update failed, internal server error';
            break;
        default:
            echo 'Unknown status code received: ' . $statusCode;
            break;
    };
} catch (\Exception $e) {
    echo 'An error occurred while creating client: ' . $e->getMessage();
}
