<?php
require __DIR__ . '/../../../autoload.php';

use GrandSlam\TalismanEsbConnector\Models\Client;
use GrandSlam\TalismanEsbConnector\Service\ClientService;
use GrandSlam\TalismanEsbConnector\EsbConnector;

$esbConnector = new EsbConnector(
    'privatekey.pem',
    'development',
    'http://talisman-redesign-esb.dev.local/'
);

$clientService = new ClientService();
$clientService->setEsbConnector($esbConnector);

$client = new Client();

$client->setEmailAddress('mbrandt@talisman.nl');
$client->setFirstName('Monique');
$client->setLastName('Brandt');
$client->setGender('female');
$client->setStreet('Weena');
$client->setHouseNumber('290');
$client->setLocationAddendum('');
$client->setCity('Rotterdam');
$client->setCountry(1);
$client->setZipCode('3221 AH'); // Space is mandatory in Dutch zipcodes
$client->setSubscribeNewsletter(true);
$client->setSubscribeBrochures(true);

try {
    $clientId = $clientService->create($client);
    echo 'Client created with ID: ' . $clientId;
} catch (Exception $e) {
    echo 'An error occurred while creating client: ' . $e->getMessage();
}
