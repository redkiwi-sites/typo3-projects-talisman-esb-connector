<?php
namespace GrandSlam\TalismanEsbConnector\Service;

/**
 * Service: Booker
 *
 * @package GrandSlam\TalismanEsbConnector\Esb\Service
 */
class BookerService extends AbstractConnectorService
{

    /**
     * @param array $parameters
     * @return array
     * @throws \Exception
     */
    public function get(array $parameters = [])
    {
        $response = $this->makeRequest($parameters, 'data/booker');
        $jsonEncodedResponse = json_encode($response);
        return json_decode($jsonEncodedResponse, true);
    }

}
