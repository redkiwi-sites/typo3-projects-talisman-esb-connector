<?php
namespace GrandSlam\TalismanEsbConnector\Service;

use GrandSlam\TalismanEsbConnector\Models\Content;

/**
 * Service: Content
 *
 * @package GrandSlam\TalismanEsbConnector\Esb\Service
 */
class ContentService extends AbstractConnectorService
{

    /**
     * @param array $parameters
     * @return array
     * @throws \Exception
     */
    public function get(array $parameters = [])
    {
        $response = $this->makeRequest($parameters, 'data/content');
        $returnArray = [];

        foreach ($response->additionalData->entries->entry as $contentEntry) {
            $content = new Content();

            $content->setTravelStudioId((int)$contentEntry->travelStudioId);
            $content->setLongName((string)$contentEntry->longName);
            $content->setShortName((string)$contentEntry->shortName);
            $content->setLongitude((float)$contentEntry->longitude);
            $content->setLatitude((float)$contentEntry->latitude);
            $content->setTravelStudioGeoTreeId((int)$contentEntry->travelStudioGeoTreeId);

            $returnArray[] = $content;
        }
        return $returnArray;
    }

}
