<?php
namespace GrandSlam\TalismanEsbConnector\Service;

use GrandSlam\TalismanEsbConnector\Models\PromotionalPackage;

/**
 * Service: Promotional
 *
 * @package GrandSlam\TalismanEsbConnector\Esb\Service
 */
class PromotionalService extends AbstractConnectorService
{

    /**
     * @param array $parameters
     * @return array
     * @throws \Exception
     */
    public function get(array $parameters = [])
    {
        $response = $this->makeRequest($parameters, 'data/getpromotionalpackages');
        $returnArray = [];

        foreach ($response->additionalData->entries->entry as $promotionalPackage) {
            $package = new PromotionalPackage();
            $package->setId((int)$promotionalPackage->id);
            $package->setName((string)$promotionalPackage->name);
            $package->setTotalPriceIncludingTax((float)$promotionalPackage->totalPriceIncludingTax);
            $returnArray[] = $package;
        }
        return $returnArray;
    }

}
