<?php
namespace GrandSlam\TalismanEsbConnector\Service;

use GrandSlam\TalismanEsbConnector\EsbConnector;

/**
 * Service: ESB service
 *
 * @package GrandSlam\TalismanEsbConnector\Service
 */
class AbstractConnectorService
{

    /**
     * @var EsbConnector
     */
    protected $esbConnector;

    /**
     * @param EsbConnector $esbConnector
     * @return void
     */
    public function setEsbConnector(EsbConnector $esbConnector)
    {
        $this->esbConnector = $esbConnector;
    }

    /**
     * @param array $parameters
     * @param string $method
     * @return \SimpleXMLElement
     * @throws \Exception
     */
    public function makeRequest(array $parameters = [], $method)
    {
        $clientRequest = $this->esbConnector->request($method, $parameters);
        if ($clientRequest['info']['http_code'] !== 200) {
            throw new \Exception('Request ' . $method . ' failed with status code ' . $clientRequest['info']['http_code']);
        }
        return new \SimpleXMLElement($clientRequest['response']);
    }

}
