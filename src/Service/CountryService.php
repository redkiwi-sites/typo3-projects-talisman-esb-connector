<?php
namespace GrandSlam\TalismanEsbConnector\Service;

use GrandSlam\TalismanEsbConnector\Models\Country;

/**
 * Service: Country
 *
 * @package GrandSlam\TalismanEsbConnector\Esb\Service
 */
class CountryService extends AbstractConnectorService
{

    /**
     * @param array $parameters
     * @return array
     * @throws \Exception
     */
    public function get(array $parameters = [])
    {
        $response = $this->makeRequest($parameters, 'data/getcountries');
        $returnArray = [];

        foreach ($response->additionalData->countries->country as $country) {
            $newCountry = new Country();
            $newCountry->setId((int)$country->id);
            $newCountry->setIsoCode((string)$country->isoCode);
            $newCountry->setName((string)$country->name);
            $returnArray[] = $newCountry;
        }

        return $returnArray;
    }

}
