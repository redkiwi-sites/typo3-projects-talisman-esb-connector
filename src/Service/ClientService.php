<?php
namespace GrandSlam\TalismanEsbConnector\Service;

use GrandSlam\TalismanEsbConnector\Models\Client;
use GrandSlam\TalismanEsbConnector\Models\Note;
use GrandSlam\TalismanEsbConnector\Models\Brochure;

/**
 * Service: Client
 *
 * @package GrandSlam\TalismanEsbConnector\Esb\Service
 */
class ClientService extends AbstractConnectorService
{

    /**
     * @param Client $client
     * @return int
     * @throws \Exception
     */
    public function create(Client $client)
    {
        $params = [
            'emailAddress' => $client->getEmailAddress(),
            'firstName' => $client->getFirstName(),
            'middleName' => $client->getMiddleName(),
            'lastName' => $client->getLastName(),
            'gender' => $client->getGender(),
            'dateOfBirth' => $client->getDateOfBirth(),
            'subscribeNewsletter' => $client->isSubscribeNewsletter(),
            'street' => $client->getStreet(),
            'houseNumber' => $client->getHouseNumber(),
            'locationAddendum' => $client->getLocationAddendum(),
            'city' => $client->getCity(),
            'country' => $client->getCountry(),
            'zipCode' => $client->getZipCode()
        ];

        $response = $this->makeRequest($params, 'data/createclient');
        return (int)$response->additionalData->clientId;
    }

    /**
     * @param Client $client
     * @return int
     * @throws \Exception
     */
    public function update(Client $client)
    {
        $params = [
            'clientId' => $client->getId(),
            'emailAddress' => $client->getEmailAddress(),
            'firstName' => $client->getFirstName(),
            'middleName' => $client->getMiddleName(),
            'lastName' => $client->getLastName(),
            'gender' => $client->getGender(),
            'dateOfBirth' => $client->getDateOfBirth(),
            'subscribeNewsletter' => $client->isSubscribeNewsletter(),
            'street' => $client->getStreet(),
            'houseNumber' => $client->getHouseNumber(),
            'locationAddendum' => $client->getLocationAddendum(),
            'city' => $client->getCity(),
            'Country' => $client->getCountry(),
            'zipCode' => $client->getZipCode()
        ];

        $response = $this->makeRequest($params, 'data/updateclient');
        return (int)$response->statusCode;
    }

    /**
     * @param Brochure $brochure
     * @return mixed
     * @throws \Exception
     */
    public function addBrochure(Brochure $brochure)
    {
        $params = [
            'clientId' => $brochure->getClientId(),
            'brochureSubject' => $brochure->getBrochureSubject(),
            'brochureText' => htmlspecialchars($brochure->getBrochureText()),
            'componentIds' => implode(',', $brochure->getComponentIds())
        ];

        $response = $this->makeRequest($params, 'data/addbrochuretoclient');
        if ((int)$response->statusCode !== 200) {
            throw new \Exception('Failed to add brochure to client, status code: ' . $response->statusCode);
        } else {
            return $response->additionalData->brochureId;
        }
    }

    /**
     * @param $clientId
     * @param Note $note
     * @return mixed
     * @throws \Exception
     */
    public function addNote($clientId, Note $note)
    {
        $params = [
            'clientId' => (int)$clientId,
            'noteType' => $note->getNoteType(),
            'noteSubject' => $note->getNoteSubject(),
            'noteText' => htmlspecialchars($note->getNoteText()),
            'requestedDate' => $note->getRequestedDate()
        ];

        $response = $this->makeRequest($params, 'data/addnotetoclient');
        if ((int)$response->statusCode !== 200) {
            throw new \Exception('Failed to add note to client, status code: ' . $response->statusCode);
        } else {
            return true;
        }
    }

    /**
     * @param $emailAddress
     * @return mixed
     */
    public function checkClientExistence($emailAddress)
    {
        $params = [
            'emailAddress' => $emailAddress
        ];
        $response = $this->makeRequest($params, 'data/checkclientexistence');
        if ((int)$response->statusCode !== 200) {
            return false;
        } else {
            $returnArray = [];
            foreach ($response->additionalData->clientIds->clientId as $client) {
                $returnArray[] = $client;
            }
            return $returnArray;
        }
    }

}
