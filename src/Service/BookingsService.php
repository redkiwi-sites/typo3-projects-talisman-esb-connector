<?php
namespace GrandSlam\TalismanEsbConnector\Service;

/**
 * Service: Bookings
 *
 * @package GrandSlam\TalismanEsbConnector\Esb\Service
 */
class BookingsService extends AbstractConnectorService
{

    /**
     * @param array $parameters
     * @return array
     * @throws \Exception
     */
    public function get(array $parameters = [])
    {
        $response = $this->makeRequest($parameters, 'data/bookings');
        $jsonEncodedResponse = json_encode($response);
        return json_decode($jsonEncodedResponse, true);
    }

}
