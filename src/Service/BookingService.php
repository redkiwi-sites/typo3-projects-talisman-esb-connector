<?php
namespace GrandSlam\TalismanEsbConnector\Service;

/**
 * Service: Booking
 *
 * @package GrandSlam\TalismanEsbConnector\Esb\Service
 */
class BookingService extends AbstractConnectorService
{

    /**
     * @param array $parameters
     * @return array
     * @throws \Exception
     */
    public function get(array $parameters = [])
    {
        $response = $this->makeRequest($parameters, 'data/booking');
        $jsonEncodedResponse = json_encode($response);
        return json_decode($jsonEncodedResponse, true);
    }

}
