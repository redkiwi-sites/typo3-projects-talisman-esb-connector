<?php
namespace GrandSlam\TalismanEsbConnector\Service;

/**
 * Service: Updated bookings
 *
 * @package GrandSlam\TalismanEsbConnector\Esb\Service
 */
class UpdatedBookingsService extends AbstractConnectorService
{

    /**
     * @param array $parameters
     * @return array
     * @throws \Exception
     */
    public function get(array $parameters = [])
    {
        $response = $this->makeRequest($parameters, 'data/updatedbookings');
        $jsonEncodedResponse = json_encode($response);
        return json_decode($jsonEncodedResponse, true);
    }

}
