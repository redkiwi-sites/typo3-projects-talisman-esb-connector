<?php
namespace GrandSlam\TalismanEsbConnector\Service;

use GrandSlam\TalismanEsbConnector\Models\GeoTreeCountry;
use GrandSlam\TalismanEsbConnector\Models\GeoTreeContinent;

/**
 * Service: Geotree
 *
 * @package GrandSlam\TalismanEsbConnector\Esb\Service
 */
class GeoTreeService extends AbstractConnectorService
{

    /**
     * @return array
     * @throws \Exception
     */
    public function get(array $parameters = [])
    {
        $response = $this->makeRequest($parameters, 'data/geotree');
        $returnArray = [];
        
        foreach ($response->additionalData->GEOTree->entry as $geoTree) {
            $continent = new GeoTreeContinent();
            $countries = [];

            $continent->setTravelStudioId((int)$geoTree->travelStudioId);
            $continent->setName((string)$geoTree->name);
            $continent->setLatitude((float)$geoTree->latitude);
            $continent->setLongitude((float)$geoTree->longitude);

            foreach ($geoTree->children->entry as $child) {
                $country = new GeoTreeCountry();

                $country->setTravelStudioId((int)$child->travelStudioId);
                $country->setName((string)$child->name);
                $country->setLatitude((float)$child->latitude);
                $country->setLongitude((float)$child->longitude);

                $countries[] = $country;
            }
            $continent->setChildren($countries);
            $returnArray[] = $continent;
        }
        return $returnArray;
    }

}
