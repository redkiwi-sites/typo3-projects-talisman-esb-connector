<?php
namespace GrandSlam\TalismanEsbConnector\Service;

/**
 * Service: Updated bookers
 *
 * @package GrandSlam\TalismanEsbConnector\Esb\Service
 */
class UpdatedBookersService extends AbstractConnectorService
{

    /**
     * @param array $parameters
     * @return array
     * @throws \Exception
     */
    public function get(array $parameters = [])
    {
        $response = $this->makeRequest($parameters, 'data/updatedbookers');
        $jsonEncodedResponse = json_encode($response);
        return json_decode($jsonEncodedResponse, true);
    }

}
