<?php
namespace GrandSlam\TalismanEsbConnector;

/**
 * Connector: ESB
 *
 * @package GrandSlam\TalismanEsbConnector
 */
class EsbConnector
{

    /**
     * @var string
     */
    protected $privateKey;

    /**
     * @var string
     */
    protected $apiKey;

    /**
     * @var string
     */
    protected $baseUrl;

    /**
     * @param $privateKeyFilePath
     * @param $apiKey
     * @param $baseUrl
     * @throws \Exception
     */
    public function __construct($privateKeyFilePath, $apiKey, $baseUrl)
    {
        if (file_exists($privateKeyFilePath)) {
            $cert = file_get_contents($privateKeyFilePath);
            if (!$cert) {
                throw new \Exception('Error: Unable to read private key file. Path ' . $privateKeyFilePath);
            } else {
                $this->privateKey = $cert;
            }
        }
        $this->apiKey = $apiKey;
        $this->baseUrl = $baseUrl;
    }

    /**
     * @param string $service
     * @param array $parameters
     * @return array
     */
    public function request($service, array $parameters = [])
    {
        $requestBody = $this->generateRequestBody($parameters);
        $requestSignature = $this->generateRequestSignature($requestBody);

        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $this->baseUrl . $service,
            CURLOPT_POST => 1,
            CURLOPT_POSTFIELDS => [
                'request' => $requestBody,
                'signature' => $requestSignature
            ]
        ]);

        $response = curl_exec($curl);
        $info = curl_getinfo($curl);
        curl_close($curl);

        return [
            'response' => $response,
            'info' => $info
        ];
    }

    /**
     * @param array $parameters
     * @return string
     */
    protected function generateRequestBody(array $parameters = [])
    {
        $xml = new \SimpleXMLElement('<?xml version="1.0" encoding="utf-8" ?><request></request>');
        $xml->addChild('apiKey', $this->apiKey);

        $xmlParameters = $xml->addChild('parameters');
        foreach ($parameters as $parameter => $value) {
            if (is_array($value)) {
                $parent = $xmlParameters->addChild($parameter);
                foreach ($value as $subKey => $subValue) {
                    if (is_array($subValue)) {
                        foreach ($subValue as $finalKey => $finalValue) {
                            $parent->addChild($finalKey, $finalValue);
                        }
                    } else {
                        $parent->addChild($subKey, $subValue);
                    }
                }
            } else {
                $xmlParameters->addChild($parameter, $value);
            }
        }
        return $xml->asXML();
    }

    /**
     * @param string $requestBody
     * @return string
     */
    protected function generateRequestSignature($requestBody)
    {
        $signature = '';
        openssl_sign($requestBody, $signature, $this->privateKey);
        return base64_encode($signature);
    }
}
