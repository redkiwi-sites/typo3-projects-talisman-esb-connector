<?php
namespace GrandSlam\TalismanEsbConnector\Models;

/**
 * Model: Brochure
 *
 * @package GrandSlam\TalismanEsbConnector\Models
 */
class Brochure
{

    /**
     * @var int
     */
    protected $clientId;

    /**
     * @var string
     */
    protected $brochureSubject;

    /**
     * @var string
     */
    protected $brochureText;

    /**
     * @var array
     */
    protected $componentIds;

    /**
     * @return int
     */
    public function getClientId()
    {
        return $this->clientId;
    }

    /**
     * @param int $clientId
     * @return void
     */
    public function setClientId($clientId)
    {
        $this->clientId = $clientId;
    }

    /**
     * @return string
     */
    public function getBrochureSubject()
    {
        return $this->brochureSubject;
    }

    /**
     * @param string $brochureSubject
     * @return void
     */
    public function setBrochureSubject($brochureSubject)
    {
        $this->brochureSubject = $brochureSubject;
    }

    /**
     * @return string
     */
    public function getBrochureText()
    {
        return $this->brochureText;
    }

    /**
     * @param string $brochureText
     * @return void
     */
    public function setBrochureText($brochureText)
    {
        $this->brochureText = $brochureText;
    }

    /**
     * @return array
     */
    public function getComponentIds()
    {
        return $this->componentIds;
    }

    /**
     * @param array $componentIds
     * @return void
     */
    public function setComponentIds($componentIds)
    {
        $this->componentIds = $componentIds;
    }

}
