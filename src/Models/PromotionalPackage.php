<?php
namespace GrandSlam\TalismanEsbConnector\Models;

/**
 * Model: PromotionalPackage
 *
 * @package GrandSlam\TalismanEsbConnector\Models
 */
class PromotionalPackage
{

    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var double
     */
    private $totalPriceIncludingTax;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return void
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return void
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return double
     */
    public function getTotalPriceIncludingTax()
    {
        return $this->totalPriceIncludingTax;
    }

    /**
     * @param double $totalPriceIncludingTax
     * @return void
     */
    public function setTotalPriceIncludingTax($totalPriceIncludingTax)
    {
        $this->totalPriceIncludingTax = $totalPriceIncludingTax;
    }

}
