<?php
namespace GrandSlam\TalismanEsbConnector\Models;

/**
 * Model: Country
 *
 * @package GrandSlam\TalismanEsbConnector\Models
 */
class Country
{

    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $isoCode;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return void
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return void
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getIsoCode()
    {
        return $this->isoCode;
    }

    /**
     * @param string $isoCode
     * @return void
     */
    public function setIsoCode($isoCode)
    {
        $this->isoCode = $isoCode;
    }

}
