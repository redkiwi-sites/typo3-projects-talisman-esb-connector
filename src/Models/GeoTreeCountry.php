<?php
namespace GrandSlam\TalismanEsbConnector\Models;

/**
 * Model: GeoTreeCountry
 *
 * @package GrandSlam\TalismanEsbConnector\Models
 */
class GeoTreeCountry
{

    /**
     * @var int
     */
    private $travelStudioId;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $latitude;

    /**
     * @var string
     */
    private $longitude;

    /**
     * @return int
     */
    public function getTravelStudioId()
    {
        return $this->travelStudioId;
    }

    /**
     * @param int $travelStudioId
     * @return void
     */
    public function setTravelStudioId($travelStudioId)
    {
        $this->travelStudioId = $travelStudioId;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return void
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * @param string $latitude
     * @return void
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;
    }

    /**
     * @return string
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * @param string $longitude
     * @return void
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;
    }

}
