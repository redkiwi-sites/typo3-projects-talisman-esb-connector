<?php
namespace GrandSlam\TalismanEsbConnector\Models;

/**
 * Model: Content
 *
 * @package GrandSlam\TalismanEsbConnector\Models
 */
class Content
{

    /**
     * @var int
     */
    private $travelStudioId;

    /**
     * @var string
     */
    private $longName;

    /**
     * @var string
     */
    private $shortName;

    /**
     * @var string
     */
    private $longitude;

    /**
     * @var string
     */
    private $latitude;

    /**
     * @var int
     */
    private $travelStudioGeoTreeId;

    /**
     * @return int
     */
    public function getTravelStudioId()
    {
        return $this->travelStudioId;
    }

    /**
     * @param int $travelStudioId
     * @return void
     */
    public function setTravelStudioId($travelStudioId)
    {
        $this->travelStudioId = $travelStudioId;
    }

    /**
     * @return string
     */
    public function getLongName()
    {
        return $this->longName;
    }

    /**
     * @param string $longName
     * @return void
     */
    public function setLongName($longName)
    {
        $this->longName = $longName;
    }

    /**
     * @return string
     */
    public function getShortName()
    {
        return $this->shortName;
    }

    /**
     * @param string $shortName
     * @return void
     */
    public function setShortName($shortName)
    {
        $this->shortName = $shortName;
    }

    /**
     * @return string
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * @param string $longitude
     * @return void
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;
    }

    /**
     * @return string
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * @param string $latitude
     * @return void
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;
    }

    /**
     * @return int
     */
    public function getTravelStudioGeoTreeId()
    {
        return $this->travelStudioGeoTreeId;
    }

    /**
     * @param int $travelStudioGeoTreeId
     * @return void
     */
    public function setTravelStudioGeoTreeId($travelStudioGeoTreeId)
    {
        $this->travelStudioGeoTreeId = $travelStudioGeoTreeId;
    }

}
