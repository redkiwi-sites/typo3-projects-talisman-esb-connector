<?php
namespace GrandSlam\TalismanEsbConnector\Models;

/**
 * Model: Note
 *
 * @package GrandSlam\TalismanEsbConnector\Models
 */
class Note
{

    /**
     * @var string
     */
    private $noteType;

    /**
     * @var string
     */
    private $noteSubject;

    /**
     * @var string
     */
    private $noteText;

    /**
     * @var int
     */
    private $requestedDate;

    /**
     * @return string
     */
    public function getNoteType()
    {
        return $this->noteType;
    }

    /**
     * @param string $noteType
     * @return void
     */
    public function setNoteType($noteType)
    {
        $this->noteType = $noteType;
    }

    /**
     * @return string
     */
    public function getNoteSubject()
    {
        return $this->noteSubject;
    }

    /**
     * @param string $noteSubject
     * @return void
     */
    public function setNoteSubject($noteSubject)
    {
        $this->noteSubject = $noteSubject;
    }

    /**
     * @return string
     */
    public function getNoteText()
    {
        return $this->noteText;
    }

    /**
     * @param string $noteText
     * @return void
     */
    public function setNoteText($noteText)
    {
        $this->noteText = $noteText;
    }

    /**
     * @return int
     */
    public function getRequestedDate()
    {
        return $this->requestedDate;
    }

    /**
     * @param int $requestedDate
     * @return void
     */
    public function setRequestedDate($requestedDate)
    {
        $this->requestedDate = $requestedDate;
    }

}
