<?php
namespace GrandSlam\TalismanEsbConnector\Models;

/**
 * Model: Client
 *
 * @package GrandSlam\TalismanEsbConnector\Models
 */
class Client
{

    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $emailAddress;

    /**
     * @var string
     */
    protected $firstName;

    /**
     * @var string
     */
    protected $middleName;

    /**
     * @var string
     */
    protected $lastName;

    /**
     * @var int
     */
    protected $dateOfBirth;

    /**
     * @var bool
     */
    protected $subscribeNewsletter;

    /**
     * @var string
     */
    protected $street;

    /**
     * @var string
     */
    protected $houseNumber;

    /**
     * @var string
     */
    protected $locationAddendum;

    /**
     * @var string
     */
    protected $city;

    /**
     * @var int
     */
    protected $country;

    /**
     * @var string
     */
    protected $zipCode;

    /**
     * @var string
     */
    protected $gender;

    /**
     * @var bool
     */
    protected $subscribeBrochures;

    /**
     * @return boolean
     */
    public function isSubscribeBrochures()
    {
        return $this->subscribeBrochures;
    }

    /**
     * @param boolean $subscribeBrochures
     * @return void
     */
    public function setSubscribeBrochures($subscribeBrochures)
    {
        $this->subscribeBrochures = $subscribeBrochures;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return void
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getEmailAddress()
    {
        return $this->emailAddress;
    }

    /**
     * @param string $emailAddress
     * @return void
     */
    public function setEmailAddress($emailAddress)
    {
        $this->emailAddress = $emailAddress;
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     * @return void
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }

    /**
     * @return string
     */
    public function getMiddleName()
    {
        return $this->middleName;
    }

    /**
     * @param string $middleName
     * @return void
     */
    public function setMiddleName($middleName)
    {
        $this->middleName = $middleName;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     * @return void
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    }

    /**
     * @return int
     */
    public function getDateOfBirth()
    {
        return $this->dateOfBirth;
    }

    /**
     * @param int $dateOfBirth
     * @return void
     */
    public function setDateOfBirth($dateOfBirth)
    {
        $this->dateOfBirth = $dateOfBirth;
    }

    /**
     * @return boolean
     */
    public function isSubscribeNewsletter()
    {
        return $this->subscribeNewsletter;
    }

    /**
     * @param boolean $subscribeNewsletter
     * @return void
     */
    public function setSubscribeNewsletter($subscribeNewsletter)
    {
        $this->subscribeNewsletter = $subscribeNewsletter;
    }

    /**
     * @return string
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * @param string $street
     * @return void
     */
    public function setStreet($street)
    {
        $this->street = $street;
    }

    /**
     * @return string
     */
    public function getHouseNumber()
    {
        return $this->houseNumber;
    }

    /**
     * @param string $houseNumber
     * @return void
     */
    public function setHouseNumber($houseNumber)
    {
        $this->houseNumber = $houseNumber;
    }

    /**
     * @return string
     */
    public function getLocationAddendum()
    {
        return $this->locationAddendum;
    }

    /**
     * @param string $locationAddendum
     * @return void
     */
    public function setLocationAddendum($locationAddendum)
    {
        $this->locationAddendum = $locationAddendum;
    }

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param string $city
     * @return void
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * @return int
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param int $country
     * @return void
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * @return string
     */
    public function getZipCode()
    {
        return $this->zipCode;
    }

    /**
     * @param string $zipCode
     * @return void
     */
    public function setZipCode($zipCode)
    {
        $this->zipCode = $zipCode;
    }

    /**
     * @return string
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * @param string $gender
     * @return void
     */
    public function setGender($gender)
    {
        $this->gender = $gender;
    }

}
